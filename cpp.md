# c++ notes
im following this [**free** corse](https://www.learncpp.com)
# chapter 0 (history)
# chapter 1 (basicly basic) 
> A computer program is a sequence of instructions that tell the computer what to do. A **statement** is a type of instruction that causes the program to perform some action.  

so a statement is a command for a computer to follow.

syntax is like grammer if you misspell a word or statement you get a syntax error.  
using a LSP (lanugae server protocol) will help you solve errors while you are coding not at compile time.  

---
### comments 
comments have nothing to do with the code its just text in the coding file its self. why you compile a file with comments they are ignored complealy.  
```cpp
//i am a regular comment 
/*im a j    uu
multi
line 
comment
*/
```
you can also use comments to "comment out" code for debuging and for fixing problems  
```cpp
int x{};  // you can also put them here too
//int y{};
```
---
### varible assignment 
computer what what is called ram (random access memory) its just abunch of computer chips that can hold **alot** ones and zeros we can store the value of varibles there

when we **declare** a varible the computer will make space for it in the ram the type of the varible will dictate how large the space is and how it is interptied.
there are many ways we can define and declare varibles  
define = saying somthing exsist and explicting saying what it is
declare = saying somthing exsist but not saying what it is
```cpp
int a; //(default initialization) this declares a ininterger called a and sets it with no value.
```
```cpp
int a = 5;//(copy initialization)
```

```cpp
int a(5);//(direct initialization)
```

```cpp
int a = 5, b = 6;          // copy initialization
int c( 7 ), d( 8 );        // direct initialization
int e { 9 }, f { 10 };     // direct brace initialization
int g = { 9 }, h = { 10 }; // copy brace initialization
int i {}, j {};            // value initialization
```
```cpp
//if you dont use a var the compiler might error
[[mabye_unused]] int x { 5 };
```

---
### iosteam
iosteam is a cpp standand libary with abunch of helpful functions however it can cause bloat naming concliioins and complexity.
include iostream and you get a few functions 
```cpp
#include <iostream>
std::cout << "words";//prints text or the value of var to the console 
// std::cout is also buffered
std::cin >> x;// reads a line from the dafasdfaskljfklsjdklonsole and saves it to a varible 
std::endl// the same as \n but it flushe the buffer
```
### undefined bahavior 
```cpp
int x; // un-init-ed int called x
printf("%i",x); // then we prnt x with could be any thing
```
### whilespace
whitespace is:  
    tabs  
     &  
s p a c e s   
you need whitespace in:
```cpp 
int x;
```
other that stuff like that it doesnt matter
```cpp
int        
x 
= 
5 


;// valid
```
### literals and opteraters
a literal is something explicit like the number 5 or the string "Hello"  
> In mathematics, an operation is a process involving zero or more input values (called operands) that produces a new value (called an output value). The specific operation to be performed is denoted by a symbol called an operator.
--- 
operators
> In mathematics, an operation is a process involving zero or more input values (called operands) that produces a new value (called an output value). The specific operation to be performed is denoted by a symbol called an operators
eg.. + - * / == <= >= != new delete throw
# chapter 2 
### functions 
functions are used so you dont have to repeat code over and over it eaier to write it once 
eg..
```cpp
// return-type name parmaters
void sayhello(){
    printf("hello");
}
// ...

sayhello();
```

the return type says what the function is going to return like int would return and int a string would return a string and void would return nothing.
eg ..
```cpp
int add(int x{}, int y{}){
    return x + y;
}
```
return is the keyword to end the function and return the givin value.

functions have to be **defined** before the main function so the program knows what it is.  
or you could **declare** it in the beggening of the program and define it later.
eg..
```cpp
int add(int x{}, int y{});
int main(){
    total = add(5, 5);
    return 0;
}
int add(int x{}, int y{}){
    return x + y;
}

```
---
### local scope
local scope talks about how depending where a varible or function in declared if it avabile
eg ..
```cpp
void yes(){
    int x{}; // x is created here
} // x dies here

x = 99; // wont work because x is only avabile in the function yes
```
---
### namespaces 

namespaces are used to avoid naming concliioins. they used the (::) operator.
iostream is an name space so the var and functions in it dont interfear with your program 
(name)::(the function) eg.. std::cout 

--- 
### multi-file 



to be revisied in future ^


# chapter 5 constant and strings 
## 5.1 constant varibles 
there are two types of constant  

- one is a name constant which means the value of the named constant will not and cannot change durring run time 
- another is a literal constant like pi (3.14) pi will never change 

types of named constants

- constant varibles
- object like macros like #define this "that"
- enumerated constants in lesssion 13.2

to make a constant varible is is your prefix varible defeination with const eg 
```cpp
const int {89};
```
the do nots:

- int const this{};
- void voo(const int){}
- use #define as const unless programing on arduiono / imbeded to save on binay space

the do's 

- prefer const over macros

const is a type qualifer as so is volatile which is rarely used.

## 5.2 Constant expressions, compile-time const, and runtime const

the as-if rule say the the compiler may change your program in order to product more optimized code.

there are 3 differnt stages of constsants

- constant expressions 
- compile time constants
- run time constants

consider the following
```cpp
#include <iostream>

int main()
{
	int x { 3 + 4 };
	std::cout << x << '\n';

	return 0;
}
```
x is equal to the constant expression 4 + 3. no matter what 4 + 3 is always equal to 7. so instead of spending cpu cycles on solving 4 + 3 every run time the compiler can just solve it then replace 4 + 3 with 7 eg
```cpp
#include <iostream>

int main()
{
	int x { 7 };
	std::cout << x << '\n';

	return 0;
}
```
there is still one more optimation that the compiler could make. since int x is only used once we could replace every accurance of x with its value to save on having to create space in ram and cpu cycles to read form ram eg
```cpp
#include <iostream>

int main()
{
	std::cout << 7 << '\n';

	return 0;
}
``` 
## 5.2 Constexpr variables
constexpr int x{};  
constexpr tells the compiler that the varible is a compile time const and will error if its not. not all data types are functonal with constexpr
## 5.4 literals


|type |value | data type |
|--- | --- | ---|
|integer value|5, 0, -3|int|
|boolean value|true, false	|bool|
|floating point value|1.2, 0.0, 3.4|double (not float!)|	
|character|‘a’, ‘\n’|char|
|C-style string|“Hello, world!”|const char[14]|


Literal suffixes

|Data type|	Suffix|	Meaning|
| --- | --- | --- |
|integral|	u or U|	unsigned int|
|integral|	l or L|	long|
|integral|	ul, uL, Ul, UL, lu, lU, Lu, LU|	unsigned long|
|integral|	ll or LL|	long long|
|integral|	ull, uLL, Ull, ULL, llu, llU, LLu, LLU|	unsigned long long|
|integral|	z or Z|	The signed version of std::size_t (C++23)|
|integral|	uz, uZ, Uz, UZ, zu, zU, Zu, ZU|	std::size_t (C++23)|
|floating point|	f or F|	float|
|floating point|	l or L|	long double|
|string|	s|	std::string|
|string|	sv|	std::string_view|

**best practice**  
- prefer upper case suffixes
- dont use magic numbers use constexpr varibles instead
## 5.5 
we use the deciaml number system but there are others like:
- octal 1 2 3 4 5 6 7 (skip any 8 or 9) 10 11 12 13 14 15 16 17 (skip any 8 or 9) 20
- hexadecimal 1 2 3 4 5 6 7 8 9 10 A B C D E F 11 12 13 14 15 16 17 18 19 10 A B C D E F 

octals:
```cpp
int octal{012};// = 10 in decimal prefix any octal with 0
```
hexadecimal:
```cpp
int hexideciamal(0x28fb); // = something
```
there is also binary which you prefix with 0b

and you could use ' to sepreate large numbers or binary 

2'132'645'785

std::cout by default outputs decimal but you can chage that 
```cpp
    int x { 12 };
    std::cout << x << '\n'; // decimal (by default)
    std::cout << std::hex << x << '\n'; // hexadecimal
    std::cout << x << '\n'; // now hexadecimal
    std::cout << std::oct << x << '\n'; // octal
    std::cout << std::dec << x << '\n'; // return to decimal
    std::cout << x << '\n'; // decimal

```

Outputting values in binary is a little harder, as std::cout doesn’t come with this capability built-in. Fortunately, the C++ standard library includes a type called std::bitset that will do this for us (in the <bitset> header).

To use std::bitset, we can define a std::bitset variable and tell std::bitset how many bits we want to store. The number of bits must be a compile-time constant. std::bitset can be initialized with an integral value (in any format, including decimal, octal, hex, or binary).

```cpp
#include <bitset> // for std::bitset
#include <iostream>

int main()
{
	// std::bitset<8> means we want to store 8 bits
	std::bitset<8> bin1{ 0b1100'0101 }; // binary literal for binary 1100 0101
	std::bitset<8> bin2{ 0xC5 }; // hexadecimal literal for binary 1100 0101

	std::cout << bin1 << '\n' << bin2 << '\n';
	std::cout << std::bitset<4>{ 0b1010 } << '\n'; // create a temporary std::bitset and print it

	return 0;
}
```
## 5.6 conditional operator
so you can bacicly do if else statemnts in arithmetic 

```cpp
constexpr int x{4};
constexpr int y{6};
constexpr int b{ (x > y) ? x : y};
// b is = to x if x is larger or y if y is larger
```
these are good for conditions in varible inits. there is no if else replacement for this.

**note**  
cpp prioritizes the evaluation of most operators above the evaluation of the conditional operator, it’s quite easy to write expressions using the conditional operator that don’t evaluate as expected.

